import * as express from 'express';
import { UserRouter } from './Routers/user_router';

export class Server 
{
    private app: express.Application;


    constructor()
    {
        /*
            required :
            npm install ts-node @types/express typescript cors jsonwebtoken
        */
        // create the application
        this.app = express();

        // Body parser is now replaced by 'express'
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));

        this.init_routes();

    }

    private init_routes()
    {

        this.app.use('/api/users', new UserRouter().router);
    }

    public start()
    {
        // run with http
        this.app.listen(8000);

        // run with https
        //this.httpsServer.listen(8000);
    }
}